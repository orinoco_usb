# KERNEL_PATH is the path to the Linux kernel build tree.  It is usually
# the same as the kernel tree, except when a separate directory was used
# for the binaries.  By default, we try to compile the modules for the
# currently running kernel.
KERNEL_PATH ?= $(shell readlink -f /lib/modules/`uname -r`/build)

ifeq (,$(KERNEL_PATH))
$(error Kernel tree not found - please set KERNEL_PATH)
endif

VERSION_HEADER = $(KERNEL_PATH)/include/linux/utsrelease.h
ifeq (,$(wildcard $(VERSION_HEADER)))
VERSION_HEADER = $(KERNEL_PATH)/include/linux/version.h
ifeq (,$(wildcard $(VERSION_HEADER)))
$(error Kernel in $(KERNEL_PATH) is not configured)
endif
endif

-include $(KERNEL_PATH)/.config

# Kernel Makefile doesn't always know the exact kernel version, so we
# get it from the kernel headers instead and pass it to make.
KERNELRELEASE = $(shell sed -ne 's/"//g;s/^\#define UTS_RELEASE //p' \
		  $(VERSION_HEADER))

SRCS = airport.c hermes.c hermes.h hermes_rid.h orinoco.c orinoco.h \
	orinoco_cs.c orinoco_nortel.c orinoco_pci.c orinoco_pci.h \
	orinoco_plx.c orinoco_tmd.c spectrum_cs.c orinoco_usb.c

DRV_DIR = drivers/net/wireless/orinoco

TAR = tar
DEPMOD = /sbin/depmod

SPARSE_FLAGS = -Wbitwise -Wcast-to-as -Wcontext -Wdefault-bitfield-sign \
	-Wtransparent-union -Wptr-subtraction-blows -Wundef -Wdecl \
	-Wone-bit-signed-bitfield -Wtypesign -D__CHECK_ENDIAN__

DISTFILES = $(addprefix $(DRV_DIR)/,$(SRCS) Kbuild compat.h) Makefile NEWS \
	README README.orinoco
DISTNAME = orinoco-$(VERSION)

DRV_DIR_ABS = $(shell pwd)/$(DRV_DIR)
VERSION = $(shell sed -ne 's/"//g;s/^\#define DRIVER_VERSION //p' \
	    $(DRV_DIR)/orinoco.h)

KBUILD_FLAGS = -C $(KERNEL_PATH) M=$(DRV_DIR_ABS) KERNELRELEASE=$(KERNELRELEASE)


# Unset flags enabling missing drivers
KBUILD_FLAGS += CONFIG_ADM8211=
KBUILD_FLAGS += CONFIG_AIRO=
KBUILD_FLAGS += CONFIG_AIRO_CS=
KBUILD_FLAGS += CONFIG_ARLAN=
KBUILD_FLAGS += CONFIG_ATH5K=
KBUILD_FLAGS += CONFIG_ATMEL=
KBUILD_FLAGS += CONFIG_B43=
KBUILD_FLAGS += CONFIG_B43LEGACY=
KBUILD_FLAGS += CONFIG_BCM43XX=
KBUILD_FLAGS += CONFIG_HOSTAP=
KBUILD_FLAGS += CONFIG_IPW2100=
KBUILD_FLAGS += CONFIG_IPW2200=
KBUILD_FLAGS += CONFIG_IWL3945=
KBUILD_FLAGS += CONFIG_IWL4965=
KBUILD_FLAGS += CONFIG_LIBERTAS=
KBUILD_FLAGS += CONFIG_P54_COMMON=
KBUILD_FLAGS += CONFIG_P54_PCI=
KBUILD_FLAGS += CONFIG_P54_USB=
KBUILD_FLAGS += CONFIG_PCI_ATMEL=
KBUILD_FLAGS += CONFIG_PCMCIA_ATMEL=
KBUILD_FLAGS += CONFIG_PCMCIA_NETWAVE=
KBUILD_FLAGS += CONFIG_PCMCIA_RAYCS=
KBUILD_FLAGS += CONFIG_PCMCIA_WAVELAN=
KBUILD_FLAGS += CONFIG_PCMCIA_WL3501=
KBUILD_FLAGS += CONFIG_PRISM54=
KBUILD_FLAGS += CONFIG_RT2X00=
KBUILD_FLAGS += CONFIG_RTL8180=
KBUILD_FLAGS += CONFIG_RTL8187=
KBUILD_FLAGS += CONFIG_STRIP=
KBUILD_FLAGS += CONFIG_USB_ATMEL=
KBUILD_FLAGS += CONFIG_USB_NET_RNDIS_WLAN=
KBUILD_FLAGS += CONFIG_USB_ZD1201=
KBUILD_FLAGS += CONFIG_WAVELAN=
KBUILD_FLAGS += CONFIG_ZD1211RW=

# Enable all drivers with sources if the kernel configuration allows them
KBUILD_FLAGS += CONFIG_HERMES=m

ifdef CONFIG_PPC_PMAC
KBUILD_FLAGS += CONFIG_APPLE_AIRPORT=m
endif

ifdef CONFIG_PCI
KBUILD_FLAGS += CONFIG_PLX_HERMES=m
KBUILD_FLAGS += CONFIG_PCI_HERMES=m
KBUILD_FLAGS += CONFIG_TMD_HERMES=m
KBUILD_FLAGS += CONFIG_NORTEL_HERMES=m
endif

ifdef CONFIG_PCMCIA
KBUILD_FLAGS += CONFIG_PCMCIA_HERMES=m
ifdef CONFIG_FW_LOADER
KBUILD_FLAGS += CONFIG_PCMCIA_SPECTRUM=m
endif
endif

ifdef CONFIG_USB
ifdef CONFIG_FW_LOADER
KBUILD_FLAGS += CONFIG_ORINOCO_USB=m
endif
endif

all: modules

modules:
	$(MAKE) $(KBUILD_FLAGS) modules

install: all
	$(MAKE) $(KBUILD_FLAGS) modules_install \
		INSTALL_MOD_DIR=kernel/$(DRV_DIR)
	$(DEPMOD) -ae

clean:
	rm -rf $(addprefix $(DRV_DIR)/,modules.order .*.cmd *.o *.ko *.mod.c \
	        .tmp_versions *.symvers)

check:
	$(MAKE) $(KBUILD_FLAGS) C=2 CF="$(SPARSE_FLAGS)" modules

ChangeLog: CVS/Entries
	cvs2cl

dist:
	rm -rf $(DISTNAME)
	mkdir -p $(DISTNAME)
	for f in $(DISTFILES); do \
		mkdir -p `dirname $(DISTNAME)/$$f`; \
		cp -f $$f $(DISTNAME)/$$f || exit 1; \
	done
	$(TAR) cvfz $(DISTNAME).tar.gz $(DISTNAME)
	rm -rf $(DISTNAME)

.PHONY: all clean dist modules
